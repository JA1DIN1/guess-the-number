#!/bin/bash

declare -i rand=$(( 1 + RANDOM % 99 ))
declare -i try_n
declare -a input_arr
text_1="Enter a number between 1 and 99"
text_2="Try again.\n"

for i in {0..2}; do
  (( try_n = 3 - i ))
  
  if (( i == 2 )); then
    text_1="${text_1:0:31} (you have ${try_n} more try remaining): "
    text_2="You have no more tries left. You lose."
  else
    text_1="${text_1:0:31} (you have ${try_n} more tries remaining): "
  fi

  while : ; do
    read -p "${text_1}" input
    [[ "${input}" =~ ^-?[0-9]+$ ]] \
      || { echo -e "\nEnter a valid number.\n"; continue; }
    (( input >= 1 && input <= 99 )) \
      || { echo -e "\nEntered number is out of range.\n"; continue; }
    
    if (( i != 0 )); then
      for item in "${input_arr[@]}"; do
        (( input == item )) \
          && { echo -e "\nYou have already tried this number.\n"; continue 2; }
      done
    fi

    input_arr+=("${input}")

    if (( input > rand )); then
      echo -e "\nEntered number is greater."
    elif (( input < rand )); then
      echo -e "\nEntered number is smaller."
    fi

    break
  done

  (( input == rand )) && echo -e "\nCongrats, you guessed right!" && break
  echo -e "\n${text_2}"
done
